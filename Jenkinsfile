def pkgName = 'api-telemedicina'

pipeline {
    agent any
    tools {nodejs "jenkins-nodejs-13.12.0"}

    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
    }
    options {
      gitLabConnection('gitlab-trebol')
      gitlabCommitStatus(name: 'jenkins')
      gitlabBuilds(builds: ['checkout', 'install modules', 'test', 'code quality','build','SonarQube analysis','Docker and Nexus','Deploy'])
    }

stages {
        stage ('checkout'){
            steps{
                gitlabCommitStatus(name: 'checkout') {
                    echo 'Pulling...' + env.BRANCH_NAME
                    checkout scm
                }
            }
        }
        stage ('test'){
            steps{
                 gitlabCommitStatus(name: 'test') {
                      /* sh '$(npm bin)/ng test --code-coverage' */
                 }
            }
            post {
                always {
                     echo 'ejecutando fichero de pruebas unitarias'
                }
            }
        }
        /* stage ('code quality'){
            steps{
                 gitlabCommitStatus(name: 'code quality') {
                      sh '$(npm bin)/ng lint'
                 }
               
            }
        } */
        /* stage('SonarQube analysis') {
            steps {
                  gitlabCommitStatus(name: 'SonarQube analysis') {
                    script{
                        def scannerHome = tool 'sonar-scanner';
                        withSonarQubeEnv('Sonar Desarrollo') {
                            sh "${scannerHome}/bin/sonar-scanner"
                            sleep(time:10,unit:"SECONDS")
                        }
                    }
                    script {
                        timeout(time: 3, unit: 'MINUTES') {
                            def qgate = waitForQualityGate()
                            if (qgate.status != 'OK') {
                            error "Pipeline aborted due to quality gate failure: ${qgate.status}"
                            }
                        }
                    }
                }
            }
        } */
        stage ('Docker and Nexus') {
         when {
                branch 'develop'
            }
            steps{
                 gitlabCommitStatus(name: 'Docker and Nexus') {
                        echo "Deploying on develop environment..."
                        sh "docker image build -t ${pkgName} ."
                        sh "docker tag ${pkgName} $NEXUS_REPOSITORY_URL_DEV/${pkgName}"
                        sh "docker push $NEXUS_REPOSITORY_URL_DEV/${pkgName}"
                 }
                    
            }
        }
        stage ('Deploy') {
        when {
                branch 'develop'
            }
            steps{
                  gitlabCommitStatus(name: 'Deploy') {
                      sh "kubectl delete -f kubernetes.yaml --ignore-not-found=true"
                      sh "kubectl apply -f kubernetes.yaml"
                   /*    sh '''
                      deploy=$(kubectl get deploy | grep ${pkgName}| cut -d " " -f1)
                      if [ $deploy == ${pkgName} ];
                      then
                      echo "Actualizando configmap"
                      kubectl create configmap ${pkgName} --from-env-file=environment/dev.properties --dry-run -o yaml | kubectl apply -f -
                     ''' */
                  }
            }
        }
    }
    post {
        always { 
            cleanWs()
        }
        success {
            echo 'Finalizado con exito'
            updateGitlabCommitStatus(name: 'Jenkins', state: 'success')
            slackSend (channel: '#cca-mgss',
                    color: 'good',
                    message: "*${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}"
            )
        }
        unstable {
            echo 'Algun paso del pipeline ha fallado.'
            updateGitlabCommitStatus(name: 'Jenkins', state: 'failed')
            slackSend (channel: '#cca-mgss',
                    color: 'bad',
                    message: "*${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}"
            )
        }
        failure {
            echo 'El pipeline ha fallado.'
            updateGitlabCommitStatus(name: 'Jenkins', state: 'failed')
             slackSend (channel: '#cca-mgss',
                    color: 'bad',
                    message: "*${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}"
            )
        }
        aborted {
            echo 'Algo ha cambiado en el repositorio.'
            updateGitlabCommitStatus(name: 'Jenkins', state: 'canceled')
             slackSend (channel: '#cca-mgss',
                    color: 'bad',
                    message: "*${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}"
            )
        }
    }

}