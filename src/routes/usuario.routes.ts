'use strict';

import express from 'express';
const controller = require('../controllers/index');

const app = express.Router();
app.use('/', controller.usuario);

export default app;
