'use strict';
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

const rolesValidos = {
  values: ['ADMIN_ROLE', 'USER_ROLE'],
  message: '{VALUE} no es un rol permitido',
};

const UsuarioSchema = new Schema({
  nombre: { type: String, required: [true, 'El nombre es necesario'], trim: true },
  email: { type: String, unique: true, required: [true, 'El correo es necesario'], trim: true },
  role: { type: String, required: true, default: 'USER_ROLE', enum: rolesValidos },
  updatedAt: { type: Date, required: true, default: Date.now },
  createdAt: { type: Date, required: true, default: Date.now },
  deletedAt: { type: Date, required: false, default: null },
});

module.exports = mongoose.model('SysUsuario', UsuarioSchema, 'sys_usuarios');
