'use strict';
const express = require('express');
// var bcrypt = require('bcryptjs');
const Usuario = require('../../models/schemas/usuario.schemas');

export async function getUsuario() {
  await Usuario.find({}, async (err: Error, Usuarios: any) => {
    if (err) {
      return err;
    }

    if (!Usuarios) {
      return 'No existen usuarios ingresados';
    }

    return Usuarios;
  });
}

export async function postUsuario(usuario: any) {
  const usuarioPrueba = new Usuario({
    nombre: 'prueba',
    email: 'prueba',
    role: 'ADMIN_ROLE',
  });

  await usuarioPrueba.save((err: Error, usuarioGuardado: any) => {
    if (err) {
      return err;
    }

    return usuarioGuardado;
  });
}
