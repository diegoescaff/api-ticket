// import 'reflect-metadata';

// import { SERVERPORT } from './config/env/envconfig';

// import Servidor from './config/server/app';

// const servidor = Servidor.iniciar(PORT);

// servidor.app.use(express.json());
// servidor.app.use(express.urlencoded({ extended: false }));

// const opciones: cors.CorsOptions = {
//   origin: '*',
//   credentials: false,
// };

// servidor.app.use(cors(opciones));

// // Importar rutas
// const appRoutes = require('./routes/index');

// /** Rutas */
// servidor.app.use('/procesamiento', appRoutes.test);

// servidor.iniciarServidor(() => {
//   console.log('Servidor Express Corriendo en puerto: \x1b[32m%s\x1b[0m', PORT);
// });

'use strict';
import server from './config/server/server';
import { SERVERPORT } from './config/env/envconfig';
import { conectar } from './config/db/mongodb';
const PORT: number = SERVERPORT;

server.listen(PORT, async () => {
  // base de datos
  await conectar();
  console.log('Servidor Express Corriendo en puerto: \x1b[32m%s\x1b[0m', PORT);
});
