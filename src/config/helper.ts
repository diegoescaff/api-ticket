'use strict';
/**
 * For each asincrono
 * @param  {Object}   array    [Array iterable]
 * @param  {Function} callback [función de callback]
 */
export async function asyncForEach(array: any, callback: any) {
  if (array) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }
}
