'use strict';
import express from 'express';
// import cors from 'cors';
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const server = express();

// CORS
server.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
  next();
});

// Body Parser
// parse lication/x-www-form-urlencoded
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());

// Importar rutas
const appRoutes = require('../../routes/index');

// rutas y controlladores
server.use('/procesamiento', appRoutes.test);
server.use('/usuarios', appRoutes.usuario);
export default server;
