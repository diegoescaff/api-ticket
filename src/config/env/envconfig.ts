const DBNAME: string = process.env.DBNAME || 'telemedicina';
const DBHOST: string = process.env.DBHOST || 'localhost';
const DBPORT: number = Number(process.env.DBPORT) || 5432;
const DBUSER: string = process.env.DBUSER || 'postgres';
const DBPASS: string = process.env.DBPASS || '201091xd';

const SERVERPORT: number = Number(process.env.SERVERPORT) || 3001;

const DBOPTIONS = { DBNAME, DBHOST, DBUSER, DBPASS, DBPORT };

export { DBOPTIONS, SERVERPORT };
