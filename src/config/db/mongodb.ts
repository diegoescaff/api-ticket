import { DBOPTIONS } from '../env/envconfig';
const mongoose = require('mongoose');

const conectar = async () => {
  //   try {
  //     const { DBHOST, DBNAME, DBPASS, DBUSER, DBPORT } = DBOPTIONS;
  //     await createConnection({
  //       type: 'postgres',
  //       host: DBHOST,
  //       port: DBPORT,
  //       username: DBUSER,
  //       password: DBPASS,
  //       database: DBNAME,
  //       entities: [path.join(__dirname, '../../models/entity/**/**.ts')],
  //       synchronize: true,
  //     });

  //     console.log('BASE DE DATOS : \x1b[32m%s\x1b[0m', 'CONECTADA');
  //   } catch (error) {
  //     console.log('BASE DE DATOS : \x1b[31m', ' NO CONECTADA');
  mongoose.connect('mongodb://localhost:27017/tickets', { useNewUrlParser: true, useUnifiedTopology: true }, (err: any, res: any) => {
    if (err) throw err;

    console.log('Base de datos: \x1b[32m%s\x1b[0m', 'online');
  });
};
export { conectar };
